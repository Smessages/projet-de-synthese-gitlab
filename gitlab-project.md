# Projet de Synthèse GitLab

## Objectif pédagogique
Le cahier des charges présenté ci-dessous est extrêmement simpliste. Il sert simplement de support pour synthétiser les diverses notions abordées dans le cour, sans avoir à buter sur des problèmes de code.  

Si vous souhaitez vous fixer un cahier des charges plus ambitieux ou en lien avec vos activités en cour, il est possible d'en discuter avec votre formateur. 

Pour bien s'approprier la partie Markdown, il est fortement recommandé de bien taper au clavier et d'éviter au maximum les copier-coller.

<br>

## Cahier des charges
Créez une calculatrice, idéalement en langage Python avec la possibilité de faire les opération suivantes: 
- "+" : Addition
- "-" : Soustraction
- "*" : Multiplication
- "/" : Division
- "%" : Modulo

Les opérations pourront être limitées à 2 opérandes et un opérateur.  
  
Pas besoin d'interface graphique. Une interaction avec un terminal est suffisante. Par exemple : 

```shell
> 1+3
> La réponse est: 4
>
> 5 / 2
> La réponse est: 2.5
>
> 6* 3
> La réponse est: 18
```

La gestion de la variabilité ou des erreurs dans la saisie est un plus qui peut être traitée dans un second temps.

<br>

## Environnement de travail
- Visual Studio Code (VSCode) avec les plugins :
  - "Markdown All in One"
  - "Markdown Emoji"
  - Plugins précédents (pour Python, GIT, ...)
- GitLab

## Équipe de travail
- Chaque apprenant est créateur et `Owner` de son propre projet
- Le binôme de l'apprenant est `Developer` sur le projet 
- Chaque apprenant est donc à la fois `Owner` de son propre, et `Developer` sur le projet de son binôme.
- Le formateur est `Maintainer` sur le projet

<br>

## Guide pratique
- Commencez par lire toutes les étapes suivantes

- Créez un projet `Privé`, associé à votre nom d'utilisateur avec pour Nom : `Nuum_Calc_'VOTRE NOM'` (remplacez bien sûr 'VOTRE NOM' par votre nom de famille)
  
- Ajoutez les membres (binôme et formateur avec les droits mentionnés ci-dessus)
  
- Réfléchissez à une architecture et à une démarche pour mener à bien le projet, en prenant en compte les points suivants.
  
- Paramétrez des `Labels` pertinents pour ce projet. Créez un ou plusieurs `Board` avec les colonnes correspondant à votre méthodologie d'organisation (Kanban, scrum, ...) : Par exemple, en plus de `Open` et `Close`, ajoutez `In Progress` et `Review`.  Dans ce cas là, par exemple assignez-vous le développement en passant une ``Issue`` à `In Progress`; puis assignez ce ticket à votre binôme en le passant dans la colonne `Review`. Après commentaire, il pourra être ré-assigné à l'`Owner` (ou appelé dans un commentaire avec '@') pour fermeture du ticket.
  
- Mettez au moins 2 `Milestones` dont chacun aura plusieurs `Issues` que vous mettrez en place (par exemple un Milestone "PoC" avec l'addition (+) fonctionnelle, puis un Milestone "Livraison" avec le projet complet)


  > :bulb: Les tâches associées aux issues seront particulièrement courtes. Dans la vrai vie on sera plutôt sur des tâches de quelques heures à quelques jours. De la même manière, un Milestone avec normalement un ordre de grandeur de quelques semaines se résumera ici tout au plus à quelques heures.


  
- Votre développement et votre documentation sera faite sur une branche `dev` (à créer) :
  - Le code doit être propre et commenté
  
  - Chaque fonction ou fonctionnalité séparée doit faire l'objet d'un "commit" (par exemple, ne pas faire un ajout d'une fonction + un bug fix d'une autre fonction dans un même commit). Les commits doivent avoir un commentaire clair (e.g. "[Feature] Ajout de l'addition #4" ou "[BugFix] "Correctif Issue #2"). Vous pouvez créer votre propre règle de nommage dans les commentaires.
  
  - Essayez de couvrir au maximum la syntaxe Markdown pour votre documentation, même si la documentation est un peu "surfaite" pour une calculatrice !

- La dernière étape avant d'atteindre le Milestone sera un `Merge Request` (dev --> main) assigné au formateur, avec votre binôme assigné comme `Reviewer`. Dans les faits, faites le Merge vous même... mais attendez les retours des personnes assignées.
  
    > :bulb: Un merge request n'est pas forcement associé à un Milestone. On pourrait très bien faire un merge request par ``Issue`` résolue (par exemple)

    > :bulb: Dans vos commits / Issues / Merge Requests, utilisez autant que possible des [Tags de référence](https://docs.gitlab.com/ee/user/markdown.html#gitlab-specific-references) (@, #, !, %, ...)    

  - Le binôme `developer` (et éventuellement le formateur) proposera une modification (même mineure/anecdotique, pour s'exercer) dans le fil de discussion du `Merge Request` et l'assignera à l'`Owner`. A cet effet le binôme `developer` créera un nouvel `Issue` relatif au `Merge Request` ( et à l'`Issue` du `Merge` si vous en avez créé un). Le nouvel `Issue` sera assigné à l'`Owner`.


  - Le `Owner` fera la modification et assignera la vérification au `developer`. Suite à son feedback, si tout est valide, l'`Owner` finalisera le merge de la branche `dev` sur `main` (ou `master`)

  - Clôturez le `Milestone`

- Passez au `Milestone` suivant avec la même logique.


# Exercices indépendants

## Exercice 1

Chargez une liste d'`Issues` en chargeant un fichier *.cvs depuis l'interface Gitlab.

## Exercice 2 

Créez un `Issue` en envoyant un email au `Service Desk` (accessible depuis le menu `Issue`) et répondez-y par l'interface GitLab

## Exercice 3 

Si vous avez encore "faim", voyez avec le formateur pour faire du test automatique un peu en avance...

