# Installation de GitLab

[Suivez ce lien pour pour déployer un container docker avec GitLab](https://docs.gitlab.com/ee/install/docker.html) à quelques différences près, tel que listées ci-dessous:
- Changer le hostname
- utiliser gitlab-ce (au lieu de gitlab-ee)
- Routez le port 22 du container sur le port 2222 de l'hôte (comme ça le port sera toujours libre pour faire un ssh avec votre VM)


```bash
sudo docker run --detach \
  --hostname localhost \
  --publish 443:443 --publish 80:80 --publish 2222:22 \
  --name gitlab \
  --restart always \
  --volume $GITLAB_HOME/config:/etc/gitlab \
  --volume $GITLAB_HOME/logs:/var/log/gitlab \
  --volume $GITLAB_HOME/data:/var/opt/gitlab \
  --shm-size 256m \
  gitlab/gitlab-ce:latest
```

Après 1 à 2 minutes vous devriez pouvoir accéder à votre instance gitlab depuis un navigateur en tapant : 127.0.0.1

Profitez-en pour changer TOUT DE SUITE le mot de passe !


Si vous devez supprimer votre container Docker : 
```bash
sudo docker stop gitlab  # Stop le container gitlab
sudo docker rm gitlab    # Supprime le container gitlab
```
