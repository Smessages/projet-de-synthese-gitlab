# Découvrez d'autres fonctionnalités de GitLab

## Snippets
- [Pour se renseigner](https://docs.gitlab.com/ee/user/snippets.html)
- Créez un projet publique
  - Créez un Snippet interne à votre projet
  - Fouillez dans les "Settings" du projet pour n’autoriser que les membres du projet à y avoir accès
  - Vérifiez que votre binôme n'y a pas accès
  - Ajoutez votre binôme comme membre et vérifiez qu'il y a accès
  
- Créez un Snippet indépendant
- Allez dans `Menu` --> `Snippet` pour consultez vos `Snippet`. Essayez de faire la différence entre les `Snippets` indépendants et ceux liés à un projet

## Autres settings du projet
Comme vous l'avez fait à l'instant pour les `Snippets`, changez plus finement les droit d'accès à votre projet sur d'autres fonctionnalités, puis testez.

  
## Activity
- Allez dans `Menu` --> `Activity` et constatez

## User Settings (preferences)
- En haut à droite, cliquez sur la petite flèche, puis sur `preferences`. Constatez les différentes options

<br>

# Découvrez l'interface d'administration de GitLab (instance où vous êtes admin)
- Allez dans `Menu` --> `Admin` 

## Labels
- Créez des `Labels` par défaut pour votre organisation. Créez ensuite un projet et constatez les Labels par défaut dans ce projet.

## Settings
- Faites le tour des paramètres et constatez