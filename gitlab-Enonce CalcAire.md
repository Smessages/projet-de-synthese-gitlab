# Projet GitLab (Manage - Plan - Code)

Lisez en entier l'énoncé avant de commencer.  
Ce projet consiste à s'exercer sur les phases `Manage`, `Plan` et `Code` de GitLab, en créant une application (simpliste) en python. Cette application nommée `CalcAire` permettra de calculer des aires de formes élémentaires (Carré, cercle, ...). Pour se faire pour mettrez en oeuvres les outils et concepts suivants : 
- GitLab
  - Markdown et Mermaid 
  - Issues / Issue Board
  - Milestones
  - Merge Requests
- Git
  - Commit
  - Branch
  - Merge
- Python
  - Code en python3
  - Usage de [`poetry`](https://python-poetry.org/docs/basic-usage/) recommandé mais pas obligatoire




# Etapes préliminaires

- Avoir un compte GitLab
- Avoir une clef SSH entre votre ordinateur et votre compte GitLab. Si ce n'est pas fait, [suivez la procédure](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair)
- Trouver un binôme (si une personne est seule, elle rejoindra un binôme pour faire un trinôme)

# Projet à réaliser

Voici une impression d'écran du Readme du projet que vous devrez créer. Vous devrez ainsi :
- Créer le projet
- Ecrire le code 
- Ecrire le Readme (markdown et mermaid)
- Et surtout, faire tout cela en respectant les [Etapes du projet](#etapes-du-projet), pour travailler l'aspect collaboratif avec GitLab.

![Readme partie 1](img/Prj_p1.png)
![Readme partie 2](img/Prj_p2.png)

:bulb: Le fichier `.toml` n'est pas pas obligatoire. Il est créé automatiquement si vous utilisez l'outil `poetry` (que je recommande) pour créer le projet d'application. 

# Etapes du projet

## 1. **Création du projet GitLab :** (5mn)
   - Créez un nouveau projet `Privé` sur GitLab pour héberger le code. Donnez au projet le nom suivant: `CalcAire_<NOM>` (en remplaçant <NOM> par votre nom).
   - Définissez les permissions de tel sorte que:
     - l'autre apprenant soit `Developper` sur le projet.
     - l'intervenant soit `Maintainer` : `@raphael.leber` (PS: Ceci n'est pas mon compte principal)

## 2. **Organisation du projet :** (30mn)
   - Avec votre binôme, discutez d'au moins 5 labels avec les noms et couleurs de votre choix, pour vous organiser au mieux sur le statut ou le type des actions à mener.
   - Créez deux Milestones dans le projet GitLab pour suivre le développement:
     - Nommez le premier Milestone `PoC square` (Programme minimaliste avec le calcul de l'aire du carré)
     - Nommez le deuxième Milestone `Full product`.
   - Créez des tâches (`Issues`) et associez-les à l'un ou l'autre Milestone pour suivre l'avancement du projet (plutôt de que de dimenssionner des tâches de durées de l'ordre de la journée, on va plutôt dimension des tâches d'une durée de quelques minutes, pour l'exercice). Exemples :
     - Création du template de projet (tous les fichiers et dossiers)
     - Création du template de README (titres et descriptions minimalistes)
     - Création d'un code minimaliste qui calcul l'aire d'un carré
     - ...
     - Création du template de code (Architecture avec la déclaration des fonctions vides)
     - Ecriture du code d'affichage du menu
     - Ecriture du code d'interprétation du choix de l'utilisateur
     - ...
   - Assignez les tâches entre vous pour le premier Milestone
   - Demandez l'avis du formateur avant de continuer


## 3. **Clonage du projet et création de branches :** (5mn)
   - Chaque apprenant doit cloner le projet GitLab sur sa machine locale.
   - Créez une branche dev sur laquelle seront fusionnées toutes les branches de fonctionnalités
   - Créez une branche dédiée par fonctionnalité en utilisant la commande `git checkout -b <nom_de_branche>`.
   - La branche `master` (ou `main`) sera limitée aux "livraisons" de code (fin de Milestone) issues de la branche `dev` 

## 4. **Travail individuel et commits :** (n * 5-20mn)
   - Chaque apprenant travaille sur une branche spécifique et ce pour chaque fonctionnalité.
   - Réalisez les modifications nécessaires dans le code pour votre partie assignée.
   - Effectuez des commits réguliers avec des messages descriptifs pour chaque fonctionnalité ou amélioration réalisée. Vous pouvez utiliser les références gitlab pour enrichir vos messages (e.g symboles # ! ~ % ...)

## 5. **Push des branches et création de Merge Requests :** (n * 5mn)
   - Une fois que vous avez terminé vos modifications sur votre branche, poussez les changements vers GitLab en utilisant `git push origin <nom_de_branche>`.
   - Sur GitLab, allez dans votre projet et créez une Merge Request pour fusionner votre branche avec la branche principale (généralement `main` ou `master`).
   - Sélectionnez l'autre apprenant comme validateur et comme vérificateur (reviewer) de la Merge Request.

## 6. **Révision et vérification mutuelle :** (n * 10mn)
   - L'autre apprenant (binôme) recevra une notification de la Merge Request et devra effectuer une révision du code.
   - Effectuez des commentaires et suggérez des améliorations si nécessaire. Vous pouvez utiliser les références gitlab pour enrichir vos messages (e.g symboles # ! ~ % ...)
   - Discutez (au travers de l'interface, et bien sûr aussi à l'oral) des modifications proposées et apportez les ajustements nécessaires.
   - Une fois que tout est vérifié et approuvé, la Merge Request peut être fusionnée.
   - Si des bugs sont découverts par la suite, créez une `Issue` de correction de bug !

## 7. **Rotation des rôles :**
   - Répétez les étapes 3 à 6 autant de fois que nécessaire en changeant de rôle. Répétez le processus de création de branches, de travail individuel, de création de Merge Requests et de vérification mutuelle.
   - La fin d'un Milestone se conclut par un merge de `dev` sur `master` (ou `main`) et un [Tag de version de livrable](https://docs.gitlab.com/ee/user/project/repository/tags/)   
   - Avant de passer au 2ème Milstone, appelez le formateur pour avis

