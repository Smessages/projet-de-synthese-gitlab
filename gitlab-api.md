# Gitlab API (python)

## Pré-requis à installer

Créez un dossier pour l'exercice et placez-vous à l'intérieur :

```bash
sudo apt install python3-venv    # install venv = V(irtual)ENV 
python -m venv GitLab_venv       # create a v(irtual)env in a folder named "GitLab_venv"
source GitLab_venv/bin/activate         # configure the terminal to work in the virtual environnement set in "GitLab_venv"
pip install python-gitlab        # install the GitLab python API lib (in the virtual environnement "GitLab_venv")
pip install PyYAML               # install the YAML python lib (in the virtual environnement "GitLab_venv")
```

> :bulb: Si vous ouvrez un nouveau terminal, n'oubliez pas de "sourcer" votre environnement virtuel (cf commande source ci-dessus)

## Référence 

Pour tous les exercices suivants, consultez ce site : https://python-gitlab.readthedocs.io/en/stable/

## Exercice 1

- Sur votre interface Gitlab (cf cour), créez un token valable 2 jours
- Testez le code suivant en ajoutant le fichier token.yaml selon les indications en commentaire. Cet exercice peut-être fait sur l'instance gitlab publique ou sur votre instance locale (par défaut, cf commentaire python) :

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Raphaël LEBER"

import gitlab
import yaml

class GitLabApiTest:

    """
    Talk with your gitlab instance
    """    

    def __init__(self):

        # Get tocken to access gitlab account
        # format expected in token.yaml:   token_gitlab: gl*********************
        with open('token.yaml') as file:
            token = yaml.load(file, Loader=yaml.FullLoader)
            self.token = token['token_gitlab']

        #self.gl = gitlab.Gitlab('https://gitlab.com', private_token=self.token)
        self.gl = gitlab.Gitlab('http://127.0.0.1', private_token=self.token)


    def create_group(self, name):
        self.group_id = self.gl.groups.create({'name': name, 'path': name}).id           


    def check_group(self, name):
        return self.gl.groups.get(name)

if __name__ == "__main__":
    GLAT = GitLabApiTest()
    GLAT.create_group("Test001")
    print( GLAT.check_group("Test001") )
```

Ce code créé un groupe, puis cherche les groupes du même nom et affiche leurs détails.

## Exercice 2

Créez un code permettant de créer l’arborescence suivante :  

- Groupe_ExAPI
  - Groupe_1
    - Projet_10
    - Projet_11
    - Projet_12
    - Projet_13
    - Projet_14
  - Groupe_2
    - Projet_20
    - Projet_21
    - Projet_22
    - Projet_23
    - Projet_24
  

## Exercice 3

Améliorez votre code afin d'assigner votre binôme comme `developer` aux projets impaires du groupe 1 et aux projets pairs du groupe 2 (cf exercice 2).

## Exercice 4

Améliorez votre code afin d'ajoutez une liste de 10 issues au projet Projet_10

## Exercice 5

Créez un code permettant de notifier chaque minute, la liste des **nouveaux** `todos` (TO DO, c'est à dire des tâches qui vous incombent comme des `Issues` ou des `Merges Requests`), en affichant à chaque fois le nom du projet associé et le nom de l'`Issue` 

